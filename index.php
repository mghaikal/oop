<!DOCTYPE html>
<html lang="en">
<?php
    require ('animal.php');
    require ('frog.php');
    require ('ape.php');

echo "<br>";
    
$sheep = new Animal("shaun");
$sheep->name; // "shaun"
$sheep->legs; // 4
$sheep->cold_blooded; // "no"

echo "<br>";

echo "<br>";

$kodok = new Frog("buduk");
$kodok->name;
$kodok->legs;
$kodok->cold_blooded;
$kodok->jump();

echo "<br>";

echo "<br>";

$sungokong = new Ape("kera sakti");

$sungokong->name;
$sungokong->legs;
$sungokong->cold_blooded;
$sungokong->yell();

echo "<br>";


?>


</html>